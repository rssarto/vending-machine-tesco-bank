package machine;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import machine.common.MachineItem;
import machine.common.exception.NoCoinIdentifiedException;
import machine.common.exception.OutOfServiceException;
import machine.common.exception.RunOutOfItemsException;
import machine.common.exception.UnsufficentMoneyException;
import machine.enums.ItemSelector;
import machine.enums.Money;
import machine.services.MoneyService;
import machine.services.Warehouse;

/**
 * Encapsulates the state of a vending machine and the operations that can be performed on it
 */
@Component
public class VendingMachine {
	
	private boolean on;
	private Warehouse warehouse;
	private MoneyService moneyService;
	
	public VendingMachine(@Autowired Warehouse warehouse, @Autowired MoneyService moneyService) {
		this.warehouse = warehouse;
		this.moneyService = moneyService;
		this.on = false;
	}
	
	public boolean isOn() {
		return on;
	}
	
	public void setOn() {
		this.on = true;
	}
	
	public void setOff() {
		this.on = false;
	}
	
	public void addItem(ItemSelector selector, int itemQuantity) throws Exception{
		this.checkMachineAvaiability();
		this.warehouse.addItem(selector, itemQuantity);
	}
	
	public int getItemQuantity(ItemSelector selector) throws OutOfServiceException{
		this.checkMachineAvaiability();
		return this.warehouse.getItem(selector).getCount();
	}
	
	public MachineItem takeItem(ItemSelector selector) throws RunOutOfItemsException, UnsufficentMoneyException, OutOfServiceException{
		MachineItem selectedItem = null;
		this.checkMachineAvaiability();
		try{
			this.moneyService.withdrawMoney(this.warehouse.getItem(selector));
			selectedItem = this.warehouse.takeItem(selector);
		}catch(Exception ex){
			System.out.println(ex.getMessage());
			throw ex;
		}
		return selectedItem;
	}
	
	public double getAvaibleMoney() throws OutOfServiceException{
		this.checkMachineAvaiability();
		return this.moneyService.avaiableMoney();
	}
	
	public double insertMoney(double value) throws NoCoinIdentifiedException, OutOfServiceException{
		this.checkMachineAvaiability();
		Money money = Money.identify(value);
		return this.moneyService.addMoney(money);
	}
	
	public List<Money> coinReturn() throws OutOfServiceException{
		List<Money> returnMoney = new ArrayList<>();
		this.checkMachineAvaiability();
		returnMoney = this.moneyService.returnAvaiableMoney();
		return returnMoney;
	}
	
	private void checkMachineAvaiability() throws OutOfServiceException{
		if( !this.isOn() ){
			throw new OutOfServiceException();
		}
	}
}

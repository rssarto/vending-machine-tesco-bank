package machine.common;

import machine.enums.ItemSelector;

public class MachineItem {
	
	private double price;
	private ItemSelector selector;
	private int count;
	
	public MachineItem(double price, ItemSelector selector, int count) {
		this.price = price;
		this.selector = selector;
		this.count = count;
	}
	
	public MachineItem(MachineItem item){
		this.price = item.getPrice();
		this.selector = item.getSelector();
		this.count = item.getCount();
	}
	
	public int getCount() {
		return count;
	}

	public double getPrice() {
		return price;
	}

	public ItemSelector getSelector() {
		return selector;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public void setCount(int count) {
		this.count = count;
	}
}

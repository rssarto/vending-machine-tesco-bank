package machine.common.exception;

public class NoCoinIdentifiedException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 287745775190272747L;
	
	public NoCoinIdentifiedException(double value){
		super("No coin identified for value " + value);
	}

}

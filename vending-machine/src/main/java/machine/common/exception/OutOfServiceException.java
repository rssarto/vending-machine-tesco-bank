package machine.common.exception;

public class OutOfServiceException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8957656963607527450L;
	
	public OutOfServiceException(){
		super("The Machine is out of service.");
	}

}

package machine.common.exception;

public class RunOutOfItemsException extends Exception {

	private static final long serialVersionUID = -1347354677110975241L;
	
	public RunOutOfItemsException(){
		super("The Machine runned out of items.");
	}

}

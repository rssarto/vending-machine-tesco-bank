package machine.common.exception;

public class UnsufficentMoneyException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 945920881611326035L;
	
	public UnsufficentMoneyException(){
		super("Unsufficient money.");
	}

}

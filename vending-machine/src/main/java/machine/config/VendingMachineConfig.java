package machine.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages={"machine.servicesImpl", "machine"})
public class VendingMachineConfig {
}

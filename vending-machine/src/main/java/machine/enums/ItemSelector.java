package machine.enums;

import machine.common.MachineItem;

public enum ItemSelector {
	A{
		@Override
		public MachineItem createItem() {
			return new MachineItem(0.60, ItemSelector.A, 0);
		}
	},
	B{
		@Override
		public MachineItem createItem() {
			return new MachineItem(1.0, ItemSelector.B, 0);
		}
	},
	C{
		@Override
		public MachineItem createItem() {
			return new MachineItem(1.7, ItemSelector.C, 0);
		}
	};
	public abstract MachineItem createItem();
}

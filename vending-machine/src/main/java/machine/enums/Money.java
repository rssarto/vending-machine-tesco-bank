package machine.enums;

import machine.common.exception.NoCoinIdentifiedException;

public enum Money {
	TEN_PENCE(0.10),TWENTY_PENCE(0.20),FIFTY_PENCE(0.50),POUND(1.00);

	private double value;
	private Money(double value){
		this.value = value;
	}
	
	public double getValue(){
		return this.value;
	}

	@Override
	public String toString() {
		return "coin value: " + value;
	}
	
	public static Money identify(double value)throws NoCoinIdentifiedException{
		Money identified = null;
		for(  Money money : Money.values() ){
			if( money.getValue() == value ){
				identified = money;
			}
		}
		
		if( identified == null ){
			throw new NoCoinIdentifiedException(value);
		}
		
		return identified;
	}
	
	
}

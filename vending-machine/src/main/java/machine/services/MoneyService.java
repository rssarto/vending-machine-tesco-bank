package machine.services;

import java.util.List;

import machine.common.MachineItem;
import machine.common.exception.UnsufficentMoneyException;
import machine.enums.Money;

public interface MoneyService {
	
	abstract double addMoney(Money money);
	abstract double avaiableMoney();
	abstract double withdrawMoney(MachineItem machineItem) throws UnsufficentMoneyException;
	abstract List<Money> returnAvaiableMoney();

}

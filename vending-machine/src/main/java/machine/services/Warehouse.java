package machine.services;

import machine.common.MachineItem;
import machine.common.exception.RunOutOfItemsException;
import machine.enums.ItemSelector;

public interface Warehouse {
	abstract void addItem(ItemSelector selector, int itemQuantity) throws IllegalArgumentException;
	abstract MachineItem takeItem(ItemSelector selector) throws RunOutOfItemsException;
	abstract MachineItem getItem(ItemSelector selector);
}

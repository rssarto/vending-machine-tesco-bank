package machine.servicesImpl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Service;

import machine.common.MachineItem;
import machine.common.exception.UnsufficentMoneyException;
import machine.enums.Money;
import machine.services.MoneyService;

@Service
public class InMemoryMoneyServiceImpl implements MoneyService {
	
	private static double avaiableMoney = 0.00;

	@Override
	public double addMoney(Money money) {
		InMemoryMoneyServiceImpl.avaiableMoney += money.getValue();
		return InMemoryMoneyServiceImpl.avaiableMoney;
	}

	@Override
	public double avaiableMoney() {
		return avaiableMoney;
	}

	@Override
	public double withdrawMoney(MachineItem machineItem) throws UnsufficentMoneyException {
		if(  machineItem.getPrice() > InMemoryMoneyServiceImpl.avaiableMoney ){
			throw new UnsufficentMoneyException();
		}
		InMemoryMoneyServiceImpl.avaiableMoney = avaiableMoney - machineItem.getPrice(); 
		return InMemoryMoneyServiceImpl.avaiableMoney;
	}

	@Override
	public List<Money> returnAvaiableMoney() {
		if( !(InMemoryMoneyServiceImpl.avaiableMoney > 0) ){
			return new ArrayList<Money>();
		}
		double change = InMemoryMoneyServiceImpl.avaiableMoney;
		InMemoryMoneyServiceImpl.avaiableMoney = 0.00;
		return defineCoinsForChange(change);
	}
	
	private List<Money> defineCoinsForChange(double money){
		List<Money> coinsList = new ArrayList<>();
		this.selectCoin(coinsList, money);
		return coinsList;
	}
	
	private void selectCoin(List<Money> coinList, double value){
		if( value >= Money.POUND.getValue() ){
			coinList.add(Money.POUND);
			value -= Money.POUND.getValue();
		}else if( value >= Money.FIFTY_PENCE.getValue() ){
			coinList.add(Money.FIFTY_PENCE);
			value -= Money.FIFTY_PENCE.getValue();
		}else if( value >= Money.TWENTY_PENCE.getValue() ){
			coinList.add(Money.TWENTY_PENCE);
			value -= Money.TWENTY_PENCE.getValue();
		}else if( value >= Money.TEN_PENCE.getValue() ){
			coinList.add(Money.TEN_PENCE);
			value -= Money.TEN_PENCE.getValue();
		}
		
		if( value > 0 ){
			selectCoin(coinList, Math.round( value * 100.0 ) / 100.0);
		}
	}
}

package machine.servicesImpl;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Service;

import machine.common.MachineItem;
import machine.common.exception.RunOutOfItemsException;
import machine.enums.ItemSelector;
import machine.services.Warehouse;

@Service
public class InMemoryWarehouse implements Warehouse {
	
	private static final Map<ItemSelector, MachineItem> items = new HashMap<>();
	
	public InMemoryWarehouse(){
		Arrays.asList(ItemSelector.values()).forEach((item) -> { InMemoryWarehouse.items.put(item, item.createItem()); });
	}
	
	public void addItem(ItemSelector selector, int itemQuantity) throws IllegalArgumentException{
		if( selector == null ){
			throw new IllegalArgumentException("Selector cannot be null.");
		}
		
		if( itemQuantity < 1 ){
			throw new IllegalArgumentException("Quantiry cannot be less than 1.");
		}
		
		MachineItem item = InMemoryWarehouse.items.get(selector);
		item.setCount(item.getCount() + itemQuantity);
	}

	public MachineItem takeItem(ItemSelector selector) throws RunOutOfItemsException {
		if( selector == null ){
			throw new IllegalArgumentException("Selector cannot be null.");
		}
		
		MachineItem item = InMemoryWarehouse.items.get(selector);
		if( item.getCount() < 1 ) {
			throw new RunOutOfItemsException();
		}
		item.setCount(item.getCount() - 1);

		return new MachineItem(item);
	}

	public MachineItem getItem(ItemSelector selector) {
		return new MachineItem(InMemoryWarehouse.items.get(selector));
	}
}

package machine;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.util.Arrays;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import machine.common.exception.NoCoinIdentifiedException;
import machine.common.exception.OutOfServiceException;
import machine.common.exception.RunOutOfItemsException;
import machine.common.exception.UnsufficentMoneyException;
import machine.config.VendingMachineConfig;
import machine.enums.ItemSelector;
import machine.enums.Money;
import machine.servicesImpl.InMemoryMoneyServiceImpl;
import machine.servicesImpl.InMemoryWarehouse;

/**
 * Unit tests for {@link VendingMachine}
 */
@ContextConfiguration(classes = VendingMachineConfig.class)
@RunWith(SpringRunner.class)
public class VendingMachineTest {
	
	@Autowired
	private VendingMachine vendingMachine;
	
	@Before
	public void addItems() throws Exception{
		this.vendingMachine.setOn();
		this.addItemA();
		this.addItemB();
		this.addItemC();
	}
	
	@After
	public void turnOf(){
		this.vendingMachine.setOff();
		assertFalse(this.vendingMachine.isOn());
	}
	
	@Test
	public void defaultStateIsOff() {
		VendingMachine machine = new VendingMachine(new InMemoryWarehouse(), new InMemoryMoneyServiceImpl());
		assertFalse(machine.isOn());
	}
	
	@Test
	public void turnsOn() {
		vendingMachine.setOn();
		assertTrue(vendingMachine.isOn());		
	}
	
	/********************************************** ADD ITEMS TO MACHINE ************************************************************/
	@Test
	public void addItemA() throws Exception{
		final int addQuantity = 100;
		this.addItem(ItemSelector.A, addQuantity, this.vendingMachine);
	}

	@Test
	public void addItemB() throws Exception{
		final int addQuantity = 100;
		this.addItem(ItemSelector.B, addQuantity, this.vendingMachine);
	}
	
	@Test
	public void addItemC() throws Exception{
		final int addQuantity = 100;
		this.addItem(ItemSelector.C, addQuantity, this.vendingMachine);
	}
	
	private void addItem(final ItemSelector selector, final int itemQuantity, VendingMachine vendingMachine) throws Exception{
		int currentItemAQuantity = vendingMachine.getItemQuantity(selector);
		final int total = itemQuantity + currentItemAQuantity;
		vendingMachine.addItem(selector, itemQuantity);
		currentItemAQuantity = vendingMachine.getItemQuantity(selector);
		assertTrue(total == currentItemAQuantity);
	}
	
	/********************************************** ADD COIN 
	 * @throws NoCoinIdentifiedException 
	 * @throws OutOfServiceException ************************************************************/
	@Test
	public void addTenPence() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addMoney(Money.TEN_PENCE.getValue(), this.vendingMachine);
	}
	
	@Test
	public void addTwentyPence() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addMoney(Money.TWENTY_PENCE.getValue(), this.vendingMachine);
	}
	
	@Test
	public void addFiftyPence() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addMoney(Money.FIFTY_PENCE.getValue(), this.vendingMachine);
	}
	
	@Test
	public void addPound() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addMoney(Money.POUND.getValue(), this.vendingMachine);
	}
	
	@Test(expected=NoCoinIdentifiedException.class)
	public void addTwoPoundsCoin() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addMoney(2.00D, this.vendingMachine);
	}
	
	private void addMoney(double money, VendingMachine vendingMachine) throws NoCoinIdentifiedException, OutOfServiceException{
		double currentMoney = vendingMachine.getAvaibleMoney();
		double total = currentMoney + money;
		currentMoney = vendingMachine.insertMoney(money); 
		assertTrue(total == currentMoney);
	}
	
	/********************************************** RETURN COIN 
	 * @throws NoCoinIdentifiedException 
	 * @throws OutOfServiceException ************************************************************/
	@Test
	public void coinReturn() throws NoCoinIdentifiedException, OutOfServiceException{
		this.addFiftyPence();
		this.addTenPence();
		this.returnAvaiableMoney(this.vendingMachine);
	}
	
	private void returnAvaiableMoney(VendingMachine vendingMachine) throws OutOfServiceException{
		double currentMoneyAvaiable = vendingMachine.getAvaibleMoney();
		List<Money> returnedMoney = vendingMachine.coinReturn();
		assertTrue(currentMoneyAvaiable == sumCoins(returnedMoney));
		assertTrue(vendingMachine.getAvaibleMoney() == 0.00);
	}
	
	private double sumCoins(List<Money> coinsList){
		double sum = 0.00;
		for( Money money : coinsList ){
			System.out.println(money);
			sum += money.getValue();
		}
		return sum;
	}
	
	/********************************************** SELECT ITEM E REQUEST CHANGE 
	 * @throws NoCoinIdentifiedException 
	 * @throws OutOfServiceException ************************************************************/	
	@Test
	public void buyItemA() throws RunOutOfItemsException, UnsufficentMoneyException, NoCoinIdentifiedException, OutOfServiceException{
		this.buyItem(Arrays.asList(Money.POUND), Arrays.asList(ItemSelector.A), this.vendingMachine);
	}
	
	@Test
	public void buyItemB() throws RunOutOfItemsException, UnsufficentMoneyException, NoCoinIdentifiedException, OutOfServiceException{
		this.buyItem(Arrays.asList(Money.POUND), Arrays.asList(ItemSelector.B), this.vendingMachine);
	}
	
	@Test
	public void buyItemC() throws RunOutOfItemsException, UnsufficentMoneyException, NoCoinIdentifiedException, OutOfServiceException{
		this.buyItem(Arrays.asList(Money.POUND, Money.POUND), Arrays.asList(ItemSelector.C), this.vendingMachine);
	}
	
	@Test(expected=UnsufficentMoneyException.class)
	public void selectItemCWithNoEnoughMoney()throws RunOutOfItemsException, UnsufficentMoneyException, NoCoinIdentifiedException, OutOfServiceException{
		if( this.vendingMachine.getAvaibleMoney() > 0.00 ){
			this.coinReturn();
		}
		this.buyItem(Arrays.asList(Money.POUND), Arrays.asList(ItemSelector.C), this.vendingMachine);
	}
	
	@Test(expected=RunOutOfItemsException.class)
	public void selectItemUntilRunningOutOfItems() throws Exception{
		VendingMachine machine = new VendingMachine(new InMemoryWarehouse(), new InMemoryMoneyServiceImpl());
		machine.setOn();
		this.addItem(ItemSelector.A, 2, machine);
		this.addMoney(Money.POUND.getValue(), machine);
		while( machine.getItemQuantity(ItemSelector.A) >= 0 ){
			machine.takeItem(ItemSelector.A);
		}
	}
	
	private void buyItem(List<Money> moneyToAddList, List<ItemSelector> itemsToTakeList, VendingMachine vendingMachine)throws RunOutOfItemsException, UnsufficentMoneyException, NoCoinIdentifiedException, OutOfServiceException{
		for( Money money : moneyToAddList ){
			this.addMoney(money.getValue(), vendingMachine);
		}

		for( ItemSelector selector : itemsToTakeList ){
			int currentQuantityItemA = vendingMachine.getItemQuantity(selector);
			vendingMachine.takeItem(selector);
			assertTrue(--currentQuantityItemA == vendingMachine.getItemQuantity(selector));
		}
		
		this.returnAvaiableMoney(vendingMachine);
	}
}
